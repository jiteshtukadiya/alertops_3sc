<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alertops_3sc
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<script type="text/javascript">
		var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'alertops_3sc' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="container header-container">	
<!--
			<div class="site-branding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/alertops.svg" /></a>	
			</div>
-->
			<!-- .site-branding -->
<!--
			<a href="#" class="btn btn-link menu-toggle" aria-controls="primary-menu" aria-expanded="false">
				<span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/menu.svg" /></span><?php esc_html_e( 'Menu', 'alertops_3sc' ); ?>
			</a>
			<nav id="site-navigation" class="main-navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
						'menu_class' 	 => 'nav',
					) );
				?>

			</nav>
-->
			<!-- #site-navigation -->
			<nav class="navbar navbar-expand-md">

			
					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>
						<?php if ( is_front_page() && is_home() ) : ?>
							<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/alertops.svg" />
							</a>
						<?php else : ?>
							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/alertops.svg" />
							</a>
						
						<?php endif; ?>
						
					
					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

					
					
				<!-- The WordPress Menu goes here -->
				<a href="#" class="btn btn-link navbar-toggler" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/menu.svg" /></span><?php esc_html_e( 'Menu', 'alertops_3sc' ); ?>
				</a>

				<?php wp_nav_menu(
					array(
						'theme_location'  => 'menu-1',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav flex-md-row flex-sm-column',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'walker'          => new understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
<!-- 				<a class="btn btn-outline-primary d-none d-lg-inline-block" href="https://github.com/twbs/bootstrap/archive/v4.0.0-beta.2.zip">Try for Free</a>				 -->
			
		</nav><!-- .site-navigation -->

		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
