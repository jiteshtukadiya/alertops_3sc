<?php
/**
 * Template Name: Single Integrations
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<header class="entry-header">
						<div class="container">
							<a class="back_link px-0" href="<?php echo esc_url( home_url( '/' ) ); ?>integrations" title="All Integrations">
							<span>
								 <svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								     <g id="Mockup/Desktop/04-Integrations/02-Singe-Integration" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-81.000000, -118.000000)" stroke-linecap="round" stroke-linejoin="round">
								        <g id="Group-6" transform="translate(0.000000, 72.000000)" stroke="#0D47A1" stroke-width="2">
								            <g id="button-link" transform="translate(82.000000, 32.000000)">
								                <g id="arrow-left-blue" transform="translate(5.000000, 19.000000) scale(-1, 1) translate(-5.000000, -19.000000) translate(0.000000, 15.000000)">
								                    <path d="M0,4 L8,4" id="Shape"></path>
								                    <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
								                </g>
								            </g>
								        </g>
								    </g>
								</svg>
							</span>
							All Integrations
						</a>
						<div class="text-center">							
							<h2>AlertOps <span title="integrates with"><i class="feather arrow-both"></i></span> <?php the_title(); ?></h2>
							<figure class="mb-5">
								<?php the_post_thumbnail(); ?>	
							</figure>
							<p class="large"><?php echo(get_the_excerpt()); ?></p>
							<a href="#" class="btn btn-primary mb-3">
								Integrate <?php the_title(); ?> 
								<span>
									<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
										<g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
										<g id="Text" transform="translate(0.000000, 64.000000)">
										<g id="Group-2" transform="translate(0.000000, 232.000000)">
										<g id="Group-4">
										<g id="Group-6" transform="translate(101.000000, 12.000000)">
																                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
																                                <path d="M0,4 L8,4" id="Shape"></path>
																                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
																                            </g>
																                        </g>
																                    </g>
																                </g>
																            </g>
																        </g>
																    </g>
																</svg>
								</span>
							</a>
							<a href="#" class="btn btn-link btn-block">Start Free Trial</a>
		<!-- 					<?php the_archive_description(); ?> -->
							<?php
		// 						the_archive_title('<h1 class="entry-title">', '</h1>' );
		// 						the_archive_description( '<p class="large">', '</p>' );
							?>					
						</div>

					</div>
					</header><!-- .page-header -->
					<section class="integration_grid_content">
					<div class="container">
						<div class="row">
							<aside class="col-lg-3 col-md-12 order-md-2  order-lg-1">
								<h6 class="text-muted mt-3">Categories</h6>
								<?php
								   $args = array(
								               'taxonomy' => 'integration_category',
								               'orderby' => 'name',
								               'order'   => 'ASC'
								           );
								
								   $cats = get_categories($args);
								?>
								<div id="tag-cloud" class="tags mb-5">	
									<?php $terms = get_the_terms( $post->ID , 'integration_category' );
									if ( $terms != null ){
									    foreach( $terms as $term ) {
										    ?>
									    <a class="tag" href="#<?php echo sanitize_title_with_dashes($cat_name, $raw_title = '', $context = 'display'); ?>">
										      <!-- https://developer.wordpress.org/reference/functions/sanitize_title_with_dashes/ -->
									           <?php print $term->slug ; ?>
									      </a>
									      <?php  unset($term);
									    }
									} ?>
								
								</div>
								<div class="help_links">
									<a href="#" class="btn btn-secondary">Integration Guide</a>
									<a href="#" class="btn btn-secondary">Features</a>
								</div>
								<!--
								<?php if ( is_active_sidebar( 'integration_aside' ) ) : 
									dynamic_sidebar( 'integration_aside' );																endif; ?> -->
							</aside>
							<?php 	/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */ ?>
							<div class="col-lg-9 col-md-12 order-md-1 order-lg-2">
								<div class="entry-content">
									<?php the_content(); ?>
								</div>
								
							</div>
						</div>
					</div>
				</section>
				<?php endwhile;  ?>
			<?php else : 
			
				get_template_part( 'template-parts/content', 'none' );
			
			endif; ?>	
			<?php get_template_part( 'template-parts/content', 'cta' ); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer(); 
