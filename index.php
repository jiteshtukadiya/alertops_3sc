<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :
 			
			if ( is_home() && ! is_front_page() ) : ?>
				<header class="entry-header"> 
					<div class="container text-center">
						<h1 class="entry-title">AlertOps <?php single_post_title(); ?></h1>
						<p class="large">
							Two or three lines to explain the purpose of this blog and its unique offerings. 				
						</p>
						
					</div>
				</header><!-- .entry-header -->
			<?php
			endif; ?>

			<?php /* Start the Loop */ ?>
			
			<section class="entry-container">
				<div class="container">
					<div class="post_grid">	
						<div class="row">
							
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'template-parts/content', 'blog-list' ); ?>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			</section>


		<?php else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
