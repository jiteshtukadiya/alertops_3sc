<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			if ( have_posts() ) : the_post(); ?>

				<section class="hero">
					<div class="container">
						<div class="row">
							<div class="announcement">
								<a href="#">
								<span class="badge new">New</span>
								<span class="label">🎉READ NOW: 5 Tips for Managing your CI/CD Pipeline.</span>
								</a>
							</div>
						</div>
						<!-- Homepage Hero Text	-->
						<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
							<?php dynamic_sidebar( 'home_right_1' ); ?>															<?php endif; ?>
						</div>
					</div>
					<?php get_template_part( 'template-parts/content', 'client-list' ); ?>


				</section>
				<section class="why-us">
					<div class="container">
						<div class="row justify-content-md-center">
							<header class="col-12 col-md-8">
								<h6 class="lighter">We exist to empower each person, on every team</h6>
								<h3>Collaborate &amp; Resolve Incidents Faster</h3>
								<p>Our premise is based on the idea of multiple people across multiple teams working together to take assignments and resolve incidents faster, while giving clients, managers and stakeholders the unique ability to acknowledge incidents as they happen-by phone, sms, or email.</p>
							</header>
						</div>
						<div class="features">
							<article class="card text-center">
									<figure class="card_graphic">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/left-scene.svg" align="" alt="" />
									</figure>
									<div class="card_text">
										<h5>Bring your teams together</h5>
										<p>Notify all your key teams, managers, and stakeholders, based on severity levels, schedules, or even skillsets.</p>
									</div>
								</article>
							<article class="card text-center">
								<figure class="card_graphic">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/right-bottom-scene.svg" align="" alt="" />
								</figure>
								<div class="card_text">
									<h5>Work Less, with Workflows</h5>
									<p>Integrate your favourite tools to manage alerts, build workflows, and automate delivery process.</p>
								</div>
							</article>
							<article class="card text-center">
								<figure class="card_graphic">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/right-top.svg" align="" alt="" />
								</figure>
								<div class="card_text">
									<h5>Protect Customer Experiences</h5>
									<p>Track and improve critical business services to provide excellent customer experience..</p>
								</div>
							</article>
						</div>
					</div>
				</section>
				<section class="integrations text-center">
					<div class="container">
						<div class="row justify-content-md-center">
							<header class="col-md-8 col-12">
								<h6 class="lighter">Connect, Collaborate & Automate</h6>
								<h3>Integrations</h3>
								<p>Easily connect your monitoring, ticketing and collaboration tools to collect events, prioritize what's important, and resolve incidents to proactively manage your uptime.</p>
							</header>
						</div>
						<?php  get_template_part( 'template-parts/content', 'home-integrations' ); ?>

					</div>
				</section>
				<section class="social_proof">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-12">
								<div class="row">									
									<div class="col-md-8 col-10 offset-1 testimonials">
										<h6 class="lighter">What Customers are Saying</h6>
										<div class="quote">
											Improved response time to business critical incidents.
										</div>
										<div class="cite">
											<cite>Pete Buzzell</cite>
											<span class="desig">eCommerce Tech Manager, Wolverine Worldwide</span>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-6 col-12">
								<div class="row">
									<div class="clients">	
										<div class="col-md-8 col-10 offset-1">
											
												<h6 class="lighter">Case Study</h6>
												<h4>How the #1 Consulting company in the world, uses AlertOps&trade; to improve customer experiences</h4>
												<p>When it comes to managing major incidents, improving customer satisfaction may be just as important as time to resolution &hellip;</p>
												<a class="btn btn-link px-0" href="#">Download the Case Study
													<span>
													<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
													    <!-- Generator: Sketch 48.1 (47250) - http://www.bohemiancoding.com/sketch -->
													    <title>arrow-right-blue</title>
													    <desc>Created with Sketch.</desc>
													    <defs></defs>
													    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-998.000000, -2480.000000)" stroke-linecap="round" stroke-linejoin="round">
													        <g id="Testimonials" transform="translate(0.000000, 2224.000000)" stroke="#304FFE" stroke-width="2">
													            <g id="Clientele" transform="translate(838.000000, 72.000000)">
													                <g id="Group-4" transform="translate(0.000000, 176.000000)">
													                    <g id="arrow-right-blue" transform="translate(161.000000, 9.000000)">
													                        <path d="M0,4 L8,4" id="Shape"></path>
													                        <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
													                    </g>
													                </g>
													            </g>
													        </g>
													    </g>
													</svg></span>
												</a>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</section>
				<section class="pricing">
					<div class="container">
						<div class="row justify-content-md-center">
							<header class="col-md-8 col-12">
								<h6 class="lighter">Pricing</h6>
							</header>
						</div>
						<?php  get_template_part( 'template-parts/content', 'pricing' ); ?>

					</div>
				</section>
				<?php get_template_part( 'template-parts/content', 'cta' ); ?>

				<?php endif; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
