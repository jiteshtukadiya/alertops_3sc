<?php
/*
 * Template Name: Pricing & Plans
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post(); ?>
				<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<div class="container text-center">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							<p class="large">
								All plans start with a 14-day free trial. <br/>
								No credit card required.
							</p>
							<div class="row">
								<div class="mx-auto">
									<div class="mt-3">
										<div class="btn-fill text-uppercase primary btn-group btn-group-toggle" data-toggle="buttons">
										  <label class="btn justify-content-center  active">
										    <input type="radio" name="options" id="option1" autocomplete="off" checked> Monthly
										  </label>
										  <label class="btn justify-content-center ">
										    <input type="radio" name="options" id="option2" autocomplete="off"> Annual <span class="badge new">Save</span>
										  </label>
										</div>
<!--
										<ul class="nav nav-tabs nav-fill">
										  <li class="nav-item">
										    <a class="nav-link active" href="#">Monthly</a>
										  </li>
										  <li class="nav-item">
										    <a class="nav-link" href="#">Annual <span class="badge new">Save</span></a>
										  </li>
										</ul>
-->								
									</div>									
								</div>
							</div>
						</div>
					</header><!-- .entry-header -->
					<div class="pricing_table_container">
							<div class="container-fluid ">							
								<div class="pricing_table">
									<div class="price_matrix">
									<div class="feature_list">
										<div class="plan_snippet">
<!-- 											<h5>Features</h5>	 -->
										</div>
										<div class="feature_labels align-items-center">
											<div class="each_feature">
												Mobile apps (iOS and Android)																			<i data-feather="info"  data-toggle="tooltip" data-placement="top" title="Tooltip on top"></i>
											</div>
											<div class="each_feature">
												Email and Push Notifications
												<i data-feather="info"  data-toggle="tooltip" data-placement="top" title="Tooltip on top"></i>
											</div>
										</div>						
									</div>
									<div class="plan text-center">
										<div class="plan_snippet">
											<div class="plan_prices">
												<h6 class="text-muted">Standard</h6>
												<span class="price large_number"><small class="dolla_sign">$</small>8</span>
												<span class="text-secondary d-block plan_type mb-5">per user per month</span>
											</div>
											<div class="plan_excerpt">
												<p>For teams wanting to keep everyone in the loop.</p>
											</div>
											<div class="plan_cta justify-content-center">
												<a class="btn btn-link" href="#">Start Free Trial</a>
											</div>
										</div>
										<div class="plan_details">
											<div class="each_feature">
												<small class="text-muted d-xl-none">Mobile apps (iOS and Android)</small>
												<span class="feature_availability tick yes" title="Yes">Yes</span>
											</div>
											<div class="each_feature">
												<small class="text-muted d-xl-none">Email and Push Notifications</small>
												<span class="feature_availability" title="Unlimited">Unlimited</span>
											</div>
										</div>
									</div>
									<div class="plan recommended text-center">
										<span class="badge d-block">Recommended</span>
										<div class="plan_snippet">
											<div class="plan_prices">
												<h6 class="text-muted">Premium</h6>
												<span class="price large_number"><small class="dolla_sign">$</small>16</span>
												<span class="text-secondary d-block plan_type mb-5">per user per month</span>
											</div>
											<div class="plan_excerpt">
												<p>For businesses ready to make AlertOps the collaboration hub for their automated workflows and on-call schedules.</p>
											</div>
											<div class="plan_cta justify-content-center">
												<a class="btn btn-primary" href="#">
													Start Free Trial
													<span>
														<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
														    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
														        <g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
														            <g id="Text" transform="translate(0.000000, 64.000000)">
														                <g id="Group-2" transform="translate(0.000000, 232.000000)">
														                    <g id="Group-4">
														                        <g id="Group-6" transform="translate(101.000000, 12.000000)">
														                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
														                                <path d="M0,4 L8,4" id="Shape"></path>
														                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
														                            </g>
														                        </g>
														                    </g>
														                </g>
														            </g>
														        </g>
														    </g>
														</svg>
													</span>
												</a>
											</div>
										</div>
										<div class="plan_details">
											<div class="each_feature">
												<small class="text-muted d-xl-none">Mobile apps (iOS and Android)</small>
												<span class="feature_availability tick yes" title="Yes">Yes</span>
											</div>
											<div class="each_feature">
												<small class="text-muted d-xl-none">Email and Push Notifications</small>
												<span class="feature_availability" title="Unlimited">Unlimited</span>
											</div>
										</div>
									</div>
									<div class="plan text-center">
										<div class="plan_snippet">
											<div class="plan_prices">
												<h6 class="text-muted">Enterprise</h6>
												<span class="price large_number"><small class="dolla_sign">$</small>27</span>
												<span class="text-secondary d-block plan_type mb-5">per user per month</span>
											</div>
											<div class="plan_excerpt">
												<p>For businesses that prioritize incident resolution, and excellent customer experiences, with 24/7 priority support, SSO, and more.</p>
											</div>
											<div class="plan_cta justify-content-center">
												<a class="btn btn-link" href="#">Start Free Trial</a>
											</div>

										</div>
										<div class="plan_details">
											<div class="each_feature">
												<small class="text-muted d-xl-none">Mobile apps (iOS and Android)</small>
												<span class="feature_availability tick yes" title="Yes">Yes</span>
											</div>
											<div class="each_feature">
												<small class="text-muted d-xl-none">Email and Push Notifications</small>
												<span class="feature_availability" title="Unlimited">Unlimited</span>
											</div>
										</div>
									</div>
									<div class="plan text-center">
										<div class="plan_snippet">
											<div class="plan_prices">
												<h6 class="text-muted">Custom</h6>
												<span class="price large_number"><small class="dolla_sign">$</small>79</span>
												<span class="text-secondary d-block plan_type mb-5">per user per month</span>
											</div>
											<div class="plan_excerpt">
												<p>Explain the features of this in a nutshell.</p>
											</div>
											<div class="plan_cta justify-content-center">
												<a class="btn btn-link" href="#">Start Free Trial</a>
											</div>

										</div>
										<div class="plan_details">
											<div class="each_feature">
												<small class="text-muted d-xl-none">Mobile apps (iOS and Android)</small>
												<span class="feature_availability tick yes" title="Yes">Yes</span>
											</div>
											<div class="each_feature">
												<small class="text-muted d-xl-none">Email and Push Notifications</small>
												<span class="feature_availability" title="Unlimited">Unlimited</span>

											</div>

										</div>
									</div>
								</div>
									
								</div>
							</div>
						</div>
					<section class="testimony">
						<div class="text-center col-lg-6 offset-lg-3 col-sm-10 offset-sm-1">
							<div class="quote large">
								We have over 100 prebuilt integrations for the most popular monitoring, chat and help desk systems and are consistently adding more.
							</div>
							<div class="justify-content-center">
								<div class="cite">
									<cite>Betty Jefferson</cite>
									<span class="desig">SuperAwesome CEO, GoodCorp</span>
								</div>
							</div>
						</div>
					</section>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<section class="faq">
									<h2 class="text-center">Frequently Asked Questions</h2>
									<div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">
										<section class="questions row">
											<div class="col-lg-6 col-12">								
												<article class="question">
													<h5>How does the 14- day trial work?</h5>
													<p>Your 14 day trials starts immediately after selecting a plan, and completing the signup process–no credit card required. You’ll also have the option to switch to a different plan at any time.</p>
												</article>
												<article class="question">
													<h5>What are your payment options?</h5>
													<p>We offer both monthly and annual payment options, and accept all major credit cards. We also offer the ability to pay by paypal, or by invoice.</p>
												</article>
												<article class="question">
													<h5>Is there a limit to how many integrations I can have?</h5>
													<p>Premium and Enterprise plans have access to unlimited* inbound integrations, while the Standard plan is limited to 5 inbound integrations. Integrations also vary by plan. Enterprise offers advanced bi-directional ITSM integrations, not available in lower plans.</p>
												</article>
											</div>
											<div class="col-lg-6 col-12">										
												<article class="question">
													<h5>How do I switch or upgrade my plan?</h5>
													<p>Simply contact our support team at¨support@alertops.com or chat with us online explaining why you would like an extension.</p>
												</article>
												<article class="question">
													<h5>How many seats do I need?</h5>
													<p>A seat or user is anyone who needs access to AlertOps. The number of paid users varies depending on your organization's size and needs. Stakeholder users are available on the Enterprise plan for 50% off.</p>
												</article>
												<article class="question">
													<h5>Do you offer Startup or Non-profit pricing or price-matching?</h5>
													<p>Yes! Startup and Non-profit pricing is based on the number of users and specific alert volume. For price matching, the buyout depends on the number of users, months in the contract and the features that you currently have. Contact our sales team to get a free quote.</p>
												</article>
											</div>
											<div class="mx-auto">
												<a href="#" class="btn btn-link btn-lg">
												Read all Frequently Asked Questions
												<span>
													<svg class="icon-brand" width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
													    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
													        <g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
													            <g id="Text" transform="translate(0.000000, 64.000000)">
													                <g id="Group-2" transform="translate(0.000000, 232.000000)">
													                    <g id="Group-4">
													                        <g id="Group-6" transform="translate(101.000000, 12.000000)">
													                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
													                                <path d="M0,4 L8,4" id="Shape"></path>
													                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
													                            </g>
													                        </g>
													                    </g>
													                </g>
													            </g>
													        </g>
													    </g>
													</svg>
												</span>
											</a>
											</div>
										</section>
									</div>
								</section>
								<section class="small_print">
									<h5>Reasonable use clause:</h5>
									<p class="tiny text-muted">AlertOps’ unlimited phone and SMS notification plans are for commercial use. Usage is limited to your organization’s employees, contractors and direct customers. AlertOps is meant to be used as an IT incident alerting and IT on-call management system only.
AlertOps reserves the right to review your account and take further action if account usage is beyond normal standards, outside of permitted usage or adversely affects our operations. If we determine that you are engaging in abnormal or impermissible usage, we will use commercially reasonable efforts to inform you and provide you with the opportunity to correct the improper usage. If you fail to correct usage activity to conform to normal use, we may exercise our right to transfer your service to a more appropriate plan, charge applicable rates or suspend or terminate your service with or without notice. If we believe that our service has been used for an unlawful purpose, we may immediately terminate your service with or without notice and/or forward the relevant communication and other information to the appropriate authorities for investigation and prosecution. We reserve all of our legal rights. AlertOps reserves the right to change this Policy at any time. Changes shall become effective when a revised policy is posted to AlertOps’ websites.</p>
								</section>
							</div>
						</div>
					</div>
				</section>
				<section class="call_to_action">
					<div class="container">
						<div class="row">
							<header class="col-md-7 col-lg-7 col-12">
								<h6 class="lighter">Take the First Step</h6>
								<h3>Experience enterprise level service when
you sign up for your free trial today.</h3>
							</header>
							<div class="col-md-5 col-lg-4 offset-lg-1 col-12">
								<form>
									<div class="form-group">
									    <label for="inputEmail">Email address</label>
										<input id="inputEmail" class="form-control" type="email" name="email" placeholder="Enter your email" />
										<button type="submit" class="btn btn-primary">Start 
										<span>
										<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
										        <g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
										            <g id="Text" transform="translate(0.000000, 64.000000)">
										                <g id="Group-2" transform="translate(0.000000, 232.000000)">
										                    <g id="Group-4">
										                        <g id="Group-6" transform="translate(101.000000, 12.000000)">
										                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
										                                <path d="M0,4 L8,4" id="Shape"></path>
										                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
										                            </g>
										                        </g>
										                    </g>
										                </g>
										            </g>
										        </g>
										    </g>
										</svg>
										</span>
										</button>
									</div>
								</form>
							</div>

						</div>
<!--
						<div class="row">
						</div>
-->
					</div>
				</section>
		
			<?php endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
