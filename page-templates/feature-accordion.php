<?php
/*
 * Template Name: Accordion features
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<section class="hero">
					<div class="container">
						<div class="row">
							<div class="announcement">
								<a href="#">
								<span class="badge new">New</span>
								<span class="label">🎉AlertOps wins the 2017 App of the Year award at SXSW.</span>
								</a>
							</div>
						</div>
							<!-- Homepage Hero Text	-->
							<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
								<?php dynamic_sidebar( 'home_right_1' ); ?>															<?php endif; ?>
						</div>
					</div>
					<?php get_template_part( 'template-parts/content', 'client-list' ); ?>
				</section>
				
				<section class="why-us">
					<div class="container">
						<div class="row justify-content-md-center">
							<header class="col-12 col-md-6">
								<h6 class="lighter">Sub Title</h6>
								<h3>Why We are Trusted Worldwide</h3>
								<p>Track incident life cycles with reports and analytics to improve management procedures that proactively reduce Mean Time To Resolution.</p>
							</header>
						</div>
						<div class="features">
							<div id="carouselExampleIndicators" class="carousel slide" data-interval="false" data-ride="carousel">
								<div class="row carousel-item active"> 
								  <div class="carousel-image">
							      	<img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/team.svg" alt="First slide">
								  </div>
								  <div class="col-lg-5 col-md-6">								
									  <div data-target="#carouselExampleIndicators" data-slide-to="0">
											<div class="card text-left">
												<div class="card_text">
													<h5>Flexible Alerting For All Your Teams</h5>
													<p>Quickly send automated notifications to your teams, managers, and stakeholders based on severity levels, on-call schedules, group attributes, and escalation policies.</p>
												</div>
											</div>
										</div>
								  </div>
								</div>
								<div class="row carousel-item"> 
								  <div class="carousel-image">
								    <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/team3.svg" alt="Second slide">
								  </div>
								  <div class="col-lg-5 col-md-6">								
									<div data-target="#carouselExampleIndicators" data-slide-to="1">
									    <div class="card text-left">
										    <div class="card_text">
												<h5>Comprehensive Reporting & Analytics</h5>
												<p>Track incident life cycles and gain actionable insights to proactively reduce MTTR (Mean Time to Resolution), for better business services and excellent customer experiences.</p>
											</div>
									    </div>
								    </div>
								  </div>
								</div>
								<div class="row carousel-item"> 
									<div class="carousel-image">
									    <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/team2.svg" alt="Third slide">
									</div>
									<div class="col-lg-5 col-md-6">								
									  <div data-target="#carouselExampleIndicators" data-slide-to="2">
										    <div class="card">
											    <div class="card_text text-left">
													<h5>Flexible Alerting For All Your Teams</h5>
													<p>Quickly send automated notifications to your teams, managers, and stakeholders based on severity levels, on-call schedules, group attributes, and escalation policies.</p>
												</div>
										    </div>
									    </div>
									</div>
								</div>
							</div>
							<!--
<div id="accordion" role="tablist">
							  <div class="row">
								<div class="col-md-4">									
								    <div class="card" role="tab" id="headingOne">
										<div class="card_text text-left" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											<h5>Flexible Alerting For All Your Teams</h5>
											<p>Quickly send automated notifications to your teams, managers, and stakeholders based on severity levels, on-call schedules, group attributes, and escalation policies.</p>
										</div> 
								    </div>
								</div>  
							    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
							      <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/team2.svg" alt="Third slide">
							    </div>
							  </div>
							  <div class="row">
								<div class="col-md-4">									
								    <div class="card" role="tab" id="headingTwo">
								      <div class="card_text text-left"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
											<h5>Flexible Alerting For All Your Teams</h5>
											<p>Quickly send automated notifications to your teams, managers, and stakeholders based on severity levels, on-call schedules, group attributes, and escalation policies.</p>
									  </div> 
								    </div>
								</div>  
							    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
							      <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/team.svg" alt="Third slide">
							    </div>
							  </div>
							  <div class="row">
								<div class="col-md-4">									
								    <div class="card" role="tab" id="headingThree">
								      <div class="card_text text-left" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
											<h5>Flexible Alerting For All Your Teams</h5>
											<p>Quickly send automated notifications to your teams, managers, and stakeholders based on severity levels, on-call schedules, group attributes, and escalation policies.</p>
										</div> 
								    </div>
								</div>  
							    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
							      <img class="d-block w-100" src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/team3.svg" alt="Third slide">
							    </div>
							  </div>

							</div>
-->
						</div>
					</div>	
				</section>
				<section class="integrations text-center">
					<div class="container">
						<div class="row justify-content-md-center">
							<header class="col-md-6 col-12">
								<h6 class="lighter">REACHING WHERE YOU ARE</h6>
								<h3>Integrations</h3>
								<p>Easily connect your monitoring, ticketing and collaboration tools to collect events, prioritize what's important, and resolve incidents to proactively manage your uptime.</p>
							</header>
						</div>
						<div class="row">
							<div class="col-md-4 col-lg-2 col-6">
								<a href="#" class="card">
									<figure>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/splunk.svg" align="" alt="" />
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-lg-2 col-6">
								<a href="#" class="card">
									<figure>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/desk-logo.svg" align="" alt="" />
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-lg-2 col-6">
								<a href="#" class="card">
									<figure>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/bmc.svg" align="" alt="" />
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-lg-2 col-6">
								<a href="#" class="card">
									<figure>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/jira.svg" align="" alt="" />
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-lg-2 col-6">
								<a href="#" class="card">
									<figure>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/slack.svg" align="" alt="" />
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-lg-2 col-6">
								<a href="#" class="card">
									<figure>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/nagios.svg" align="" alt="" />
									</figure>
								</a>
							</div>
						</div>
					</div>
				</section>
<section class="social_proof">
					<div class="container">
						<div class="row">
							<div class="col-md-4 offset-md-1 col-12">
								<div class="testimonials">
									<h6 class="lighter">GOOD THINGS PEOPLE ARE SAYING</h6>
									<div class="quote">
										We have over 100 prebuilt integrations for the most popular monitoring, chat and help desk systems and are consistently adding more.
									</div>
									<div class="cite">
										<cite>Betty Jefferson</cite>
										<span class="desig">SuperAwesome CEO, GoodCorp</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-12 clients offset-md-2">
								<div class="">
									<h6 class="lighter">TRUSTED BY THOSE YOU TRUST</h6>
									<h4>More than 100 happy clients</h4>
									<p>Over last X years, we have served companies like CLIENT 1, CLIENT 2, CLIENT 3 and more with their OBJECTIVE.</p>
									<a class="btn btn-link px-0" href="#">View all Customers 
										<span>
										<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										    <!-- Generator: Sketch 48.1 (47250) - http://www.bohemiancoding.com/sketch -->
										    <title>arrow-right-blue</title>
										    <desc>Created with Sketch.</desc>
										    <defs></defs>
										    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-998.000000, -2480.000000)" stroke-linecap="round" stroke-linejoin="round">
										        <g id="Testimonials" transform="translate(0.000000, 2224.000000)" stroke="#304FFE" stroke-width="2">
										            <g id="Clientele" transform="translate(838.000000, 72.000000)">
										                <g id="Group-4" transform="translate(0.000000, 176.000000)">
										                    <g id="arrow-right-blue" transform="translate(161.000000, 9.000000)">
										                        <path d="M0,4 L8,4" id="Shape"></path>
										                        <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
										                    </g>
										                </g>
										            </g>
										        </g>
										    </g>
										</svg></span>
									</a>
								</div>
							</div>
						</div>
					</div>

				</section>
				<section class="call_to_action">
					<div class="container">
						<div class="row">
							<header class="col-md-7 col-lg-7 col-12">
								<h6 class="lighter">Take the First Step</h6>
								<h3>Experience enterprise level service when
you sign up for your free trial today.</h3>
							</header>
							<div class="col-md-5 col-lg-4 offset-lg-1 col-12">
								<form>
									<div class="form-group">
									    <label for="inputEmail">Email address</label>
										<input id="inputEmail" class="form-control" type="email" name="email" placeholder="Enter your email" />
										<button type="submit" class="btn btn-primary">Start <span>
										<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
										        <g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
										            <g id="Text" transform="translate(0.000000, 64.000000)">
										                <g id="Group-2" transform="translate(0.000000, 232.000000)">
										                    <g id="Group-4">
										                        <g id="Group-6" transform="translate(101.000000, 12.000000)">
										                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
										                                <path d="M0,4 L8,4" id="Shape"></path>
										                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
										                            </g>
										                        </g>
										                    </g>
										                </g>
										            </g>
										        </g>
										    </g>
										</svg>
										</span></button>
									</div>
								</form>
							</div>

						</div>
<!--
						<div class="row">
						</div>
-->
					</div>
				</section>
			<?php endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
