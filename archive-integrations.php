<?php
/**
 * Template Name: Integrations
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header class="entry-header">
				<div class="container text-center">
					<h1><?php echo str_replace("Archives: ", "", get_the_archive_title()); ?></h1>
					<p class="large">Two or three lines to explain the purpose of this page and its unique offerings.</p>
					<a href="#" class="btn btn-primary mb-3">
						Get Started 
						<span>
							<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								<g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
								<g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
								<g id="Text" transform="translate(0.000000, 64.000000)">
								<g id="Group-2" transform="translate(0.000000, 232.000000)">
								<g id="Group-4">
								<g id="Group-6" transform="translate(101.000000, 12.000000)">
														                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
														                                <path d="M0,4 L8,4" id="Shape"></path>
														                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
														                            </g>
														                        </g>
														                    </g>
														                </g>
														            </g>
														        </g>
														    </g>
														</svg>
						</span>
					</a>
					<p class="tiny">Start with a 14-day free trial. No credit card required.</p>
<!-- 					<?php the_archive_description(); ?> -->
					<?php
// 						the_archive_title('<h1 class="entry-title">', '</h1>' );
// 						the_archive_description( '<p class="large">', '</p>' );
					?>					
				</div>
			</header><!-- .page-header -->
			<section class="integration_grid_content">
				<div class="container">
					<div class="row">
					<?php
					if ( have_posts() ) : ?>
			
						<aside class="col-lg-3 col-md-12">
							<!--
<form  class="btn-group btn-group-vertical btn-group-toggle" data-toggle="buttons" action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
								<?php
									if( $terms = get_terms( 'integration_category', 'orderby=name' ) ) : // to make it simple I use default categories
 											
										foreach ( $terms as $term ) : 
											echo '<label class="btn btn-secondary"><input type="radio" name="filters" id="filter-option-' . $term->term_id . ' " autocomplete="off" value="' . $term->term_id . '">' . $term->name . '</label>'; // ID of the category as the value of an option
										endforeach;
									endif;
								?>
								<input type="hidden" name="action" value="myfilter">
							</form>
-->
							<?php
							   $args = array(
							               'taxonomy' => 'integration_category',
							               'orderby' => 'name',
							               'order'   => 'ASC'
							           );
							
							   $cats = get_categories($args);
							?>

							<div id="list-example" class="list-group">	
								<?php
								   $count = 0;
								   foreach($cats as $cat) {
								   $cat_name = $cat->name;
								   $count= $count + 1;
								?>
								      <a class="list-group-item list-group-item-action" href="#<?php echo sanitize_title_with_dashes($cat_name, $raw_title = '', $context = 'display'); ?>">
								           <?php echo $cat_name; ?>
								      </a>
								<?php
								} ?>
							
							</div>
							
							
							<!--
							<?php if ( is_active_sidebar( 'integration_aside' ) ) : 
								dynamic_sidebar( 'integration_aside' );																endif; ?> -->
						</aside>
						<?php 	/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */ ?>
						<div class="col-lg-9 col-md-12">
							<form class="">
								<div class="row">
									<div class="col-md-6">
										<!-- <label for="exampleInputEmail1">Email address</label> -->
										<input type="search" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Search apps" />
									</div>
									
								</div>
							</form>
							
							<div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
								<?php get_template_part( 'template-parts/content', 'featured-integrations' ); ?>
								<div id="response" class="integration_grid grid">
									<div class="row">
									<div class="col-12">
										<h4>All Integrations</h4>
									</div>
									<?php
									/*
									 * Loop through Categories and Display Posts within
									 */
		 							$post_type = 'integrations';
									$args = array(
										'post_type' => $post_type,
								        'posts_per_page' => -1,  //show all posts
								        'orderby'=> 'title', 
								        'order' => 'ASC',
								    );
									$posts = new WP_Query($args);
									
									if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post(); ?>			
								    	<div class="col-md-4 col-12 d-flex">
											<article>
												<div class="card_heading">
													<figure>
														<?php the_post_thumbnail(); ?>
													</figure>
													<p><strong><?php the_title(); ?></strong></p>
													<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
												</div>
											</article>
										</div> 
									    <!-- each integration -->
									<?php endwhile; endif; ?>
									
								</div>
								</div>
								<div class="col-lg-8 offset-lg-2 col-12">
								<div class="minor_cta">
									<h5>Become our integration partner.</h5>
									<p>We are always adding new integrations. Let us know if you would like to partner with us.</p>
									<a href="#" class="btn btn-link px-0">
										Get in touch
										<span>
											<svg class="icon-brand" width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
									<g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
									<g id="Text" transform="translate(0.000000, 64.000000)">
									<g id="Group-2" transform="translate(0.000000, 232.000000)">
									<g id="Group-4">
									<g id="Group-6" transform="translate(101.000000, 12.000000)">
															                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
															                                <path d="M0,4 L8,4" id="Shape"></path>
															                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
															                            </g>
															                        </g>
															                    </g>
															                </g>
															            </g>
															        </g>
															    </g>
															</svg>
							</span>
									</a>
								</div>
								</div>
							</div>
						</div>
			
<!--  					<?php the_posts_navigation(); ?> -->
			
			 		<?php else : 
			
						get_template_part( 'template-parts/content', 'none' );
			
					endif; ?>
				</div>
				</div>
			</section>
			<?php get_template_part( 'template-parts/content', 'cta' ); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer(); 
