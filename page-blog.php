<?php
/*
 * Template Name: Blog Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header class="entry-header">
				<div class="container text-center">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							<p class="large">
								All plans start with a 14-day free trial. <br/>
								No credit card required.
							</p>
							<div class="row">
								<div class="mx-auto">
									<div class="mt-3">
										<div class="btn-fill text-uppercase primary btn-group btn-group-toggle" data-toggle="buttons">
										  <label class="btn justify-content-center  active">
										    <input type="radio" name="options" id="option1" autocomplete="off" checked> Monthly
										  </label>
										  <label class="btn justify-content-center ">
										    <input type="radio" name="options" id="option2" autocomplete="off"> Annual <span class="badge new">Save</span>
										  </label>
										</div>
<!--
										<ul class="nav nav-tabs nav-fill">
										  <li class="nav-item">
										    <a class="nav-link active" href="#">Monthly</a>
										  </li>
										  <li class="nav-item">
										    <a class="nav-link" href="#">Annual <span class="badge new">Save</span></a>
										  </li>
										</ul>
-->								
									</div>									
								</div>
							</div>
						</div>
			</header><!-- .entry-header -->
			<?php
			while ( have_posts() ) : the_post(); ?>
				<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php get_template_part( 'template-parts/content', get_post_format() ); ?>

				</section>
			<?php endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
