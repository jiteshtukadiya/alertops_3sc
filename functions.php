<?php
/**
 * alertops_3sc functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package alertops_3sc
 */

if ( ! function_exists( 'alertops_3sc_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function alertops_3sc_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on alertops_3sc, use a find and replace
		 * to change 'alertops_3sc' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'alertops_3sc', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'alertops_3sc' ),
		) );

		// 		Adding new menus
		register_nav_menus( array(
			'menu-2' => esc_html__( 'Footer', 'alertops_3sc' ),
		) );

		// 		Upload SVG
		function cc_mime_types($mimes) {
		  $mimes['svg'] = 'image/svg+xml';
		  return $mimes;
		}
		add_filter('upload_mimes', 'cc_mime_types');

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'alertops_3sc_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Homepage Widgets

		/**
		 * Register our sidebars and widgetized areas.
		 *
		 */
		function arphabet_widgets_init() {
			register_sidebar( array(
				'name'          => 'Home Hero Area',
				'id'            => 'home_right_1',
				'before_widget' => '<div class="row">',
				'after_widget'  => '</div>',
				'before_title'  => '<h1>',
				'after_title'   => '</h1>',
			) );
		
		}
		add_action( 'widgets_init', 'arphabet_widgets_init' );
		
		// Integration Sidebar

		/**
		 * Register our sidebars and widgetized areas.
		 *
		 */
		function inte_sidebar_widgets_init() {
			register_sidebar( array(
				'name'          => 'Integration Sidebar',
				'id'            => 'integration_aside',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			) );
		
		}
		add_action( 'widgets_init', 'inte_sidebar_widgets_init' );
		
		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'alertops_3sc_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function alertops_3sc_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'alertops_3sc_content_width', 640 );
}
add_action( 'after_setup_theme', 'alertops_3sc_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function alertops_3sc_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'alertops_3sc' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'alertops_3sc' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'alertops_3sc_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function alertops_3sc_scripts() {
	wp_enqueue_style( 'alertops_3sc-style', get_stylesheet_uri() );

// 	wp_enqueue_script( 'alertops_3sc-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'alertops_3sc-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), '20151215', true );
	
	wp_enqueue_script( 'alertops_3sc-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), null, true );

	wp_enqueue_script( 'alertops_3sc-tether', 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js', array(), null, true );

	wp_enqueue_script( 'alertops_3sc-popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array(), null, true );

	wp_enqueue_script( 'alertops_3sc-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js', array(), null, true );

// 	wp_enqueue_script( 'alertops_3sc-integration', get_template_directory_uri() . '/js/integration_filter.js', array(), null, true );

	wp_enqueue_script( 'alertops_3sc-feather', 'https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js', array(), null, true );
	
	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	

}
add_action( 'wp_enqueue_scripts', 'alertops_3sc_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';




/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/widgets/home_hero.php';


/**
 *
 * Pricing Post Type
 */
require get_template_directory() . '/widgets/pricing.php';

/**
 *
 * Integrations Post Type
 */
require get_template_directory() . '/widgets/integrations.php';


/**
*
* Update Archive Title
*/
add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

/**
* Remove P tag from Archive Descripiton
*
*/
remove_filter('the_archive_description','wpautop');


/**
* Excerpt Length
*
*/

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  } 
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}

/**
* Remove P tag around images in text
*
*/

function filter_ptags_on_images($content){
  return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');