<?php 
/*
Plugin Name: Integrations
Version: 1.0
Description: Integrations
Author: Tejas bhatt
Text Domain: integrations
*/

//* Create Integration Type custom taxonomy
add_action( 'init', 'integration_type_taxonomy' );
function integration_type_taxonomy() {

	register_taxonomy( 'integration_category', 'integrations',
		array(
			'labels' => array(
				'name'          => _x( 'Types', 'taxonomy general name', 'executive' ),
				'add_new_item'  => __( 'Add New Integration Category', 'executive' ),
				'new_item_name' => __( 'New Integration Category', 'executive' ),
			),
			'exclude_from_search' => true,
			'has_archive'         => true,
			'hierarchical'        => true,
			'rewrite'             => array( 'slug' => 'integration-category', 'with_front' => false ),
			'show_ui'             => true,
			'show_tagcloud'       => false,
		)
	);

}


//* Create Integration Tags custom taxonomy
add_action( 'init', 'integration_tag_taxonomy' );
function integration_tag_taxonomy() {

	register_taxonomy( 'integration_tag', 'integrations',
		array(
			'labels' => array(
				'name'          => _x( 'Integration Tags', 'taxonomy general name', 'executive' ),
				'add_new_item'  => __( 'Add New Integration Tag', 'executive' ),
				'new_item_name' => __( 'New Integration Tag', 'executive' ),
			),
			'exclude_from_search' => true,
			'has_archive'         => true,
			'hierarchical'        => true,
			'rewrite'             => array( 'slug' => 'integration-type', 'with_front' => false ),
			'show_ui'             => true,
			'show_tagcloud'       => false,
		)
	);

}


// create post type for Integration

add_action( 'init', 'register_integrations' );
 
function register_integrations() {
 
    $labels = array(
        'name' => _x( 'Integrations', 'integrations' ),
        'singular_name' => _x( 'Integration', 'integration' ),
        'add_new' => _x( 'Add New', 'integrations' ),
        'add_new_item' => _x( 'Add New Integration', 'integrations' ),
        'edit_item' => _x( 'Edit Integration', 'integrations' ),
        'new_item' => _x( 'New Integration', 'integrations' ),
        'view_item' => _x( 'View Integration', 'integrations' ),
        'search_items' => _x( 'Search Integrations', 'integrations' ),
        'not_found' => _x( 'No Integrations found', 'integrations' ),
        'not_found_in_trash' => _x( 'No Integrations found in Trash', 'integrations' ),
        'parent_item_colon' => _x( 'Parent Integration:', 'integrations' ),
        'menu_name' => _x( 'All Integrations', 'integrations' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Integrations',
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'taxonomies'      => array( 'integration_category', 'integration_tag' ),
    );
 
    register_post_type( 'integrations', $args );
}

/*AJAX Filter*/
function filter_function(){
	$post_type = 'integrations';
	$args = array(
		'post_type' => $post_type,
		'orderby' => 'title', // we will sort posts by date
		'order'	=> 'ASC' // ASC DESC
	);
 
	// for taxonomies / categories
	if( isset( $_POST['filters'] ) ) :
		echo 'yo';
		$args = array(
// 			'post_type' => $post_type,
	        'posts_per_page' => -1,  //show all posts
	        'orderby'=> 'title', 
	        'order' => 'ASC',
	        
	    );
	else  :
		echo 'here';
	endif;
	   
	$query = new WP_Query( $args );
 
	if( $query->have_posts() ) :
		while( $query->have_posts() ): $query->the_post();
			echo '<h5>' . $query->post->post_title . '</h5>';
		endwhile;
		wp_reset_postdata();
	else :
		echo 'No posts found';
	endif;
 
	die();
}
 
 
add_action('wp_ajax_myfilter', 'filter_function'); 
add_action('wp_ajax_nopriv_myfilter', 'filter_function');