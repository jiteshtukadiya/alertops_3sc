<?php 
/*
Plugin Name: My Featured Content
Version: 1.0
Plugin URI: http://danielpataki.com
Description: Allows you to add an arbitrary featured item to the sidebar. Includes a title, image, description and a link.
Author: Daniel Pataki
Author URI: http://danielpataki.com/
Text Domain: my_featured_content
*/

add_action( 'widgets_init', 'mfc_init' );

function mfc_init() {
	register_widget( 'mfc_widget' );
}

class mfc_widget extends WP_Widget
{

public function __construct() {
    $widget_details = array(
        'classname' => 'mfc_widget',
        'description' => 'Creates a featured item consisting of a title, image, description and link.'
    );

    parent::__construct( 'mfc_widget', 'Featured Item Widget', $widget_details );

    add_action('admin_enqueue_scripts', array($this, 'mfc_assets'));
}


public function mfc_assets() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');
    wp_enqueue_script('ads_script', get_template_directory_uri() . '/js/mfc-media-upload.js', false, '1.0', true);

}


public function widget( $args, $instance )
{
	echo $args['before_widget'];
	

	?>
	<div class="content col-lg-5 col-md-12 col-12">
		<?php if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
			} 
		?>
		<div class='hero_cta'>
			<a href='<?php echo esc_url( $instance['link_url'] ) ?>' class="btn btn-primary">
			<?php echo esc_html( $instance['link_title'] ) ?>
			<span>
				<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				    <title>arrow-right</title>
				    <desc>Created with Sketch.</desc>
				    <defs></defs>
				    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
				        <g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
				            <g id="Text" transform="translate(0.000000, 64.000000)">
				                <g id="Group-2" transform="translate(0.000000, 232.000000)">
				                    <g id="Group-4">
				                        <g id="Group-6" transform="translate(101.000000, 12.000000)">
				                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
				                                <path d="M0,4 L8,4" id="Shape"></path>
				                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
				                            </g>
				                        </g>
				                    </g>
				                </g>
				            </g>
				        </g>
				    </g>
				</svg>
				</span>
		</a>
			<span class="lighter"><?php echo wpautop( esc_html( $instance['description'] ) ) ?></span>
		</div>
	</div>
	<div class="graphic col-lg-7 col-md-12 col-12">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/graphics/hero.svg" align="" alt="" />

<!-- 		<img src='<?php echo $instance['image'] ?>'> -->
	</div>

	<?php

	echo $args['after_widget'];
}

public function update( $new_instance, $old_instance ) {  
    return $new_instance;
}

public function form( $instance ) {
	$title = '';
    if( !empty( $instance['title'] ) ) {
        $title = $instance['title'];
    }

    
    $link_url = '';
    if( !empty( $instance['link_url'] ) ) {
        $link_url = $instance['link_url'];
    }

    $link_title = '';
    if( !empty( $instance['link_title'] ) ) {
        $link_title = $instance['link_title'];
    }

	$description = '';
    if( !empty( $instance['description'] ) ) {
        $description = $instance['description'];
    }

	$image = '';
	if(isset($instance['image']))
	{
	    $image = $instance['image'];
	}
    ?>
    
    <p>
        <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>


    <p>
        <label for="<?php echo $this->get_field_name( 'link_url' ); ?>"><?php _e( 'Link URL:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'link_url' ); ?>" name="<?php echo $this->get_field_name( 'link_url' ); ?>" type="text" value="<?php echo esc_attr( $link_url ); ?>" />
    </p>

    <p>
        <label for="<?php echo $this->get_field_name( 'link_title' ); ?>"><?php _e( 'Link Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'link_title' ); ?>" name="<?php echo $this->get_field_name( 'link_title' ); ?>" type="text" value="<?php echo esc_attr( $link_title ); ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_name( 'description' ); ?>"><?php _e( 'Call to Action Description:' ); ?></label>
        <textarea class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" type="text" ><?php echo esc_attr( $description ); ?></textarea>
    </p>
	<p>
        <label for="<?php echo $this->get_field_name( 'image' ); ?>"><?php _e( 'Image:' ); ?></label>
        <input name="<?php echo $this->get_field_name( 'image' ); ?>" id="<?php echo $this->get_field_id( 'image' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $image ); ?>" />
        <input class="upload_image_button" type="button" value="Upload Image" />
    </p>
    <?php
    }
}