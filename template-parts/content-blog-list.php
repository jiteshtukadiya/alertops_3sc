<?php
/**
 * Template Name: Blog List
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */
 
 ?>
 
<div class="col-lg-4 col-sm-6 col-12">
	<article id="post-<?php the_ID(); ?>" class="card">			
			<div class="card_graphic">
				<?php alertops_3sc_post_thumbnail(); ?>
			</div>
			<div class="card_text">
				<footer>
					<span class="cat-links"><?php alertops_3sc_entry_footer_list(); ?> </span>	
				</footer>
				<?php
				if ( is_singular() ) :
					the_title( '<h4 class="entry-title">', '</h4>' );
				else :
					the_title( '<h4 class="entry-title">', '</h4>' );
				endif;
		
				if ( 'post' === get_post_type() ) : ?>
				<?php
				endif; ?>
				<p><?php echo excerpt(15); ?></p>
				<div class="meta">
					<?php alertops_3sc_author(); ?>
					<span class="divider"></span>
					<?php alertops_3sc_posted_on(); ?>
				</div>
			</div>
		<a href="<?php the_permalink();?>" class="post_link"></a>
		
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
	
