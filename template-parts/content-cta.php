<?php
/**
 * Partial For Call to Action
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 * Template based on https://wordimpress.com/loop-through-categories-and-display-posts-within/
 * Changed the argument based on https://wordpress.stackexchange.com/questions/260998/get-template-part-based-on-get-post-type-for-a-custom-post-type-instead-of-g 
 */
?>

<section class="call_to_action">
	<div class="container">
		<div class="row">
			<header class="col-md-7 col-lg-7 col-12">
				<h6 class="lighter">Take the First Step</h6>
				<h3>Experience enterprise level service when
you sign up for your free trial today.</h3>
			</header>
			<div class="col-md-5 col-lg-4 offset-lg-1 col-12">
				<form>
					<div class="form-group">
					    <label for="inputEmail">Email address</label>
						<input id="inputEmail" class="form-control" type="email" name="email" placeholder="Enter your email" />
						<button type="submit" class="btn btn-primary">Start 
						<span>
						<svg width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-275.000000, -531.000000)" stroke-linecap="round" stroke-linejoin="round">
						        <g id="Hero" transform="translate(82.000000, 216.000000)" stroke="#FFFFFF" stroke-width="2">
						            <g id="Text" transform="translate(0.000000, 64.000000)">
						                <g id="Group-2" transform="translate(0.000000, 232.000000)">
						                    <g id="Group-4">
						                        <g id="Group-6" transform="translate(101.000000, 12.000000)">
						                            <g id="arrow-right" transform="translate(93.000000, 8.000000)">
						                                <path d="M0,4 L8,4" id="Shape"></path>
						                                <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
						                            </g>
						                        </g>
						                    </g>
						                </g>
						            </g>
						        </g>
						    </g>
						</svg>
						</span>
						</button>
					</div>
				</form>
			</div>

		</div>
<!--
		<div class="row">
		</div>
-->
	</div>
</section>
