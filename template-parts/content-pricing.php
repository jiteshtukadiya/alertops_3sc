<?php
/*
 * Home Integration List Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */
 ?>

<div class="row">

	<?php
	/*
	 * Loop through Categories and Display Posts within
	 */
	$post_type = 'pricing_packages';
	 
	$args = array(  
        'post_type' => $post_type, 
        'posts_per_page' => 6
	);
	$new_query = new WP_Query($args);
    while($new_query -> have_posts()) {
         
        $new_query->the_post();
        
        global $post;
        
        $meta = get_post_meta($post->ID);

        $features = json_decode($meta['_package_features'][0]);
        $color = $meta['_package_color'][0];
        $price = $meta['_package_price'][0];
        $buy_link = $meta['_package_buy_link'][0];
	?>
    <div>
        <div>
            <h2><?php echo $post->post_title; ?></h2>
            <div style="background-color: <?php echo $color;?>">$<?php echo $price; ?></div>
        </div>
        <div>
            <ul>
                <?php foreach($features as $feature) { ?>
                    <li><?php echo $feature; ?></li>
                <?php } ?>
            </ul>
        </div>
        <div>
            <a href="<?php echo $buy_link; ?>" class="btn" style="background-color: <?php echo $color;?>">Buy now</a>
        </div>
    </div>
	<?php } ?>
</div>