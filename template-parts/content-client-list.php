<?php
/*
 * Client List Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */
 ?>

<div class="client-list">
	<div class="container">
		<div class="row">
			<article>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/Ring_Central.jpg" align="" alt="" />
			</article>
			<article>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/RoyalCyber.jpg" align="" alt="" />
			</article> 
			<article>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/Tekscape.jpg" align="" alt="" />
			</article>
			<article>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/IGSEnergy.jpg" align="" alt="" />
			</article>
			<article>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/OCTO_DC_Gov.jpg" align="" alt="" />
			</article>
		</div>
	</div>
</div>