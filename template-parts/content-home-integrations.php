<?php
/*
 * Home Integration List Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */
 ?>

<div class="row">
<!--
	<?php
	/*
	 * Loop through Categories and Display Posts within
	 */
	$post_type = 'integrations';
	 
	$args = array(  
    'post_type' => $post_type, 
    'posts_per_page' => 6, 
    'tax_query' => array(
        array(
            'taxonomy' => 'integration_category',
            'field'    => 'slug', // search by slug name, you may change to use ID
	        'terms'    => 'featured-integrations', // value of the slug for taxonomy, in term using ID, you should using integer type casting (int) $value
	        ),
	    )
	);
	$new_query = new WP_Query($args);
	while($new_query -> have_posts()) : $new_query -> the_post();
	?>
	<div class="col-md-4 col-lg-2 col-6">
		<a href="#" class="card">
			<figure>
				<?php the_post_thumbnail(); ?>
			</figure>
		</a>
	</div>
	
	<?php endwhile; ?>
-->

<div class="col-md-4 col-lg-2 col-6">
		<a href="#" class="card">
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/splunk.svg" align="" alt="" />
			</figure>
		</a>
	</div>
	<div class="col-md-4 col-lg-2 col-6">
		<a href="#" class="card">
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/desk-logo.svg" align="" alt="" />
			</figure>
		</a>
	</div>
	<div class="col-md-4 col-lg-2 col-6">
		<a href="#" class="card">
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/bmc.svg" align="" alt="" />
			</figure>
		</a>
	</div>
	<div class="col-md-4 col-lg-2 col-6">
		<a href="#" class="card">
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/jira.svg" align="" alt="" />
			</figure>
		</a>
	</div>
	<div class="col-md-4 col-lg-2 col-6">
		<a href="#" class="card">
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/slack.svg" align="" alt="" />
			</figure>
		</a>
	</div>
	<div class="col-md-4 col-lg-2 col-6">
		<a href="#" class="card">
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/nagios.svg" align="" alt="" />
			</figure>
		</a>
	</div>
</div>
<div class="row justify-content-center">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>integrations" class="btn btn-link m-5">All Integrations 
		<span>
			<svg class="icon-brand" width="12px" height="10px" viewBox="0 0 12 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <!-- Generator: Sketch 48.1 (47250) - http://www.bohemiancoding.com/sketch -->
			    <title>arrow-right-blue</title>
			    <desc>Created with Sketch.</desc>
			    <defs></defs>
			    <g id="Mockup/Desktop/01-Home/01-home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-998.000000, -2480.000000)" stroke-linecap="round" stroke-linejoin="round">
			        <g id="Testimonials" transform="translate(0.000000, 2224.000000)" stroke="#304FFE" stroke-width="2">
			            <g id="Clientele" transform="translate(838.000000, 72.000000)">
			                <g id="Group-4" transform="translate(0.000000, 176.000000)">
			                    <g id="arrow-right-blue" transform="translate(161.000000, 9.000000)">
			                        <path d="M0,4 L8,4" id="Shape"></path>
			                        <polyline id="Shape" points="6 0 10 4 6 8"></polyline>
			                    </g>
			                </g>
			            </g>
			        </g>
			    </g>
			</svg>
		</span>
	</a>
</div>
