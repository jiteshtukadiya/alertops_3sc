<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>			
	<header class="entry-header" id="with-background" style="background-image: url(<?php echo get_the_post_thumbnail_url();?>;)">
		<div class="container" style="position: relative; z-index: 999;">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 col-sm-12">
					<footer class="entry-footer">
						<span class="cat-links"><?php alertops_3sc_entry_footer_list(); ?> </span>	
					</footer><!-- .entry-footer -->
					<h2><?php the_title(); ?></h2>
					<div class="meta">
						<?php alertops_3sc_author(); ?>
						<span class="divider"></span>
						<span class="posted-on"><?php the_date('M j, y'); ?></span>
					</div>

				</div>
			</div>
		</div>
	</header>
	<div class="container">
		<div class="entry-content">
			<?php the_content(); ?>
		</div><!-- .entry-content -->
		<div class="row">
			<div class="col-lg-8 offset-lg-2 col-sm-12">
				<?php get_template_part( 'template-parts/content', 'question-cta' ); ?>			
			</div>				
		</div>
	</div>				
	<?php get_template_part( 'template-parts/content', 'cta' ); ?>
	<?php get_template_part( 'template-parts/content', 'related-posts' ); ?>
</article><!-- #post-<?php the_ID(); ?> -->
