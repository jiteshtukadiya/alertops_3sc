<?php
/**
 * Template Name: Question CTA
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */
 
 ?>
 
 <section class="question_cta">
	<h4>Have questions for us?</h4>
	<p class="large">We’re always around to help you with any questions you have before you get started. Simply get in touch.</p>
	<a href="#" class="btn btn-outline-primary mb-3">Contact us</a>
	<p class="tiny">or call 844-292-8255 for more details</p>
</section>
