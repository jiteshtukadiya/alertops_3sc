<?php
/**
 * Template part for displaying integrations list in archive-integrations
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 * Template based on https://wordimpress.com/loop-through-categories-and-display-posts-within/
 * Changed the argument based on https://wordpress.stackexchange.com/questions/260998/get-template-part-based-on-get-post-type-for-a-custom-post-type-instead-of-g 
 */
?>


<?php
/*
 * Loop through Categories and Display Posts within
 */
$post_type = 'integrations';
$args = array(
	'post_type' => $post_type,
    'posts_per_page' => -1,  //show all posts
    'orderby'=> 'title', 
    'order' => 'ASC'
);
$posts = new WP_Query($args);

if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post(); ?>			
	<div class="col-md-4 col-6 d-flex">
		<article>
			<div class="card_heading">
				<figure>
					<?php the_post_thumbnail(); ?>
				</figure>
				<p><strong><?php the_title(); ?></strong></p>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
			</div>
		</article>
	</div> 
    <!-- each integration -->
<?php endwhile; endif; ?>
