<?php
/**
 * Template to display related posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 */

?>

<section class="related-posts">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h5>Also read:</h5>			
			</div>
			<?php
			//for use in the loop, list 5 post titles related to first tag on current post
			$tags = wp_get_post_tags($post->ID);
			if ($tags) {
				$first_tag = $tags[0]->term_id;
				$args=array(
					'tag__in' => array($first_tag),
					'post__not_in' => array($post->ID),
					'posts_per_page'=>5,
					'caller_get_posts'=>1
				);
				$my_query = new WP_Query($args);
				if( $my_query->have_posts() ) {
					while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<div class="col-lg-4 col-sm-6 col-12">
							<article id="post-<?php the_ID(); ?>" class="card">			
							<div class="card_graphic">
								<?php alertops_3sc_post_thumbnail(); ?>
							</div>
							<div class="card_text">

								<?php
								if ( is_singular() ) :
									the_title( '<h5 class="entry-title">', '</h5>' );
								else :
									the_title( '<h5 class="entry-title">', '</h5>' );
								endif;
						
								if ( 'post' === get_post_type() ) : ?>
								<?php
								endif; ?>
							</div>
							<a href="<?php the_permalink();?>" class="post_link" title="Permanent Link to <?php the_title_attribute(); ?>"></a>
							
						</article><!-- #post-<?php the_ID(); ?> -->
			 
						</div>
			<?php
			endwhile;
			}
			wp_reset_query();
			}
			?>
		</div>
	</div>
</section>