<?php
/**
 * Template part for displaying integrations list in archive-integrations
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alertops_3sc
 * Template based on https://wordimpress.com/loop-through-categories-and-display-posts-within/
 * Changed the argument based on https://wordpress.stackexchange.com/questions/260998/get-template-part-based-on-get-post-type-for-a-custom-post-type-instead-of-g 
 */
?>


<?php
/*
 * Loop through Categories and Display Posts within
 */
$post_type = 'integrations';
 
// Get all the taxonomies for this post type
$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
 
foreach( $taxonomies as $taxonomy ) :
 	
    // Gets every "category" (term) in this taxonomy to get the respective posts
    $terms = get_terms( $taxonomy );
	?>
		
	<?php 
    foreach( $terms as $term ) : ?>
    	<?php $int_cat_name = $term->name; ?>
    	<?php if($int_cat_name=="Featured Integrations") : ?>
    		<div id="popular" class="featured_integrations grid">
				<div class="row">
					<?php
			        $args = array(
			// 				'post_type' => $post_type,
			                'posts_per_page' => -1,  //show all posts
			                'orderby'=> 'title', 
			                'order' => 'ASC',
			                'tax_query' => array(
			                    array(
			                        'taxonomy' => $taxonomy,
			                        'field' => 'slug',
			                        'terms' => $term->slug,
			                    )
			                )
			 
			            );
			        $posts = new WP_Query($args);
			 
			        if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post(); ?>			
			           <div class="col-md-4 col-6 d-flex">
							<article class="<?php print strtolower(get_the_title()); ?>">
								<figure>
									<?php the_post_thumbnail(); ?>
								</figure>
								<div class="text">
									<h4><?php the_title(); ?></h4>
									<?php the_excerpt(); ?>									
								</div>
								<a href="<?php the_permalink(); ?> " title="<?php the_title(); ?>"></a>
							</article>
						</div> <!-- each integration -->
			        <?php endwhile; endif; ?>
				  	<?php wp_reset_query(); ?>
				</div>
			</div>
	  	<?php endif; ?>
    <?php endforeach; ?>
<?php endforeach; ?>
<?php wp_reset_query(); ?>
							

<!-- #post-<?php the_ID(); ?> -->
