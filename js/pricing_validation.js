function addPackageFeature() {

	// var elem = jQuery('input[type="hidden"]')
	// elem.val(jQuery('#package_feature').val());
	var html = '<li><input type="hidden" name="package_features[]" value="' + jQuery('#package_feature').val() + '" />';
	html += '<span>' + jQuery('#package_feature').val() + '</span>';
	html += '<a href="javascript:void(0);" class="ok" style="display: none" onclick="doneEditPackageFeature(event)">ok</a>';
	html += '<a href="javascript:void(0);" class="edit" onclick="editPackageFeature(event)">Edit</a>';
	html += '<a href="javascript:void(0);" onclick="deletePackageFeature(event)">Delete</a></li>';
	jQuery('#package_features_box').append(html);
}

function deletePackageFeature(e) {

	jQuery(e.target).parent().remove();
}

function editPackageFeature(e) {

	jQuery(e.target).parent().find('input').attr('type', 'text');
	jQuery(e.target).parent().find('span').hide();
	jQuery(e.target).parent().find('a.ok').show();
	jQuery(e.target).parent().find('a.edit').hide();
}

function doneEditPackageFeature(e) {

	jQuery(e.target).parent().find('input').attr('type', 'hidden');
	jQuery(e.target).parent().find('span').html(jQuery(e.target).parent().find('input').val()).show();
	jQuery(e.target).parent().find('a.ok').hide();
	jQuery(e.target).parent().find('a.edit').show();
}

jQuery(document).ready(function($) {
 
    $("#post-body-content").prepend('<div id="pricing_error" class="error" style="display:none" ></div>');
 
    $('#post').submit(function() {
 
        if ( $("#post_type").val() =='pricing_packages' ) {
            return wppt_validate_pricing_packages();
        }
        else if ( $("#post_type").val() =='pricing_tables' ) {
            return wppt_validate_pricing_tables();
        }
 
    });
	var wppt_validate_pricing_packages = function() {
	    var err = 0;
	    $("#pricing_error").html("");
	    $("#pricing_error").hide();
	 
	    if ( $("#title").val() == '' ) {
	        $("#pricing_error").append( "<p>Please enter Package Name.</p>" );
	        err++;
	    }
	    if ( $("#package_price").val() == '' ) {
	        $("#pricing_error").append( "<p>Please enter Package Price.</p>" );
	        err++;
	    }
	    if ( $("#package_buy_link").val() == '' ) {
	        $("#pricing_error").append( "<p>Please enter Package Buy Link.</p>" );
	        err++;
	    }
	 
	    if ( err > 0 ) {
	        $("#publish").removeClass( "button-primary-disabled" );
	        $("#ajax-loading").hide();
	        $("#pricing_error").show();
	        return false;
	    }
	    else {
	        return true;
	    }
	};
	
	var wppt_validate_pricing_tables = function() {
	    var err = 0;
	    $("#pricing_error").html("");
	    $("#pricing_error").hide();
	 
	    if ( $("#title").val() == '' ) {
	        $("#pricing_error").append( "<p>Please enter Pricing Table Name.</p>" );
	        err++;
	    }
	    if ( err > 0 ) {
	        $("#publish").removeClass( "button-primary-disabled" );
	        $("#ajax-loading").hide();
	        $("#pricing_error").show();
	        return false;
	    }
	    else {
	        return true;
	    }
	};
});