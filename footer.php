<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alertops_3sc
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
<!--				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'footer-menu',
					) );
				?> -->
				<div class="col-md-3 col-6 footer-links">
					<h6 class="light">About</h6>
					<ul>
						<li><a href="#">Link 1</a></li>
						<li><a href="#">Link 2</a></li>
						<li><a href="#">Link 3</a></li>
						<li><a href="#">Link 4</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-6  footer-links">
					<h6 class="light">Product</h6>
					<ul>
						<li><a href="#">Link 1</a></li>
						<li><a href="#">Link 2</a></li>
						<li><a href="#">Link 3</a></li>
						<li><a href="#">Link 4</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-6  footer-links">
					<h6 class="light">Resources</h6>
					<ul>
						<li><a href="#">Link 1</a></li>
						<li><a href="#">Link 2</a></li>
						<li><a href="#">Link 3</a></li>
						<li><a href="#">Link 4</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-6 footer-links">
					<h6 class="light">Help</h6>
					<ul>
						<li><a href="#">Link 1</a></li>
						<li><a href="#">Link 2</a></li>
						<li><a href="#">Link 3</a></li>
						<li><a href="#">Link 4</a></li>
					</ul>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="row justify-content-between site-stuff justify-content-sm-center">
<!-- 					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="footer-logo" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/alertops.svg" /></a> -->
					<div class="col-md-6 col-12 d-md-flex app-links mb-sm-1">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/google-play.svg" /></a>	
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/app-store.svg" /></a>	
					</div>
					<div class="col-md-6 col-12 d-md-flex align-items-center justify-content-end">
						<p class="d-flex tiny">&copy; <?php echo date(Y); ?> AlertOps, All Rights Reserved.</p>
						<ul class="d-flex social-links">
							<li><a class="facebook" href="#">facebook</a></li>
							<li><a class="twitter" href="#">twitter</a></li>
							<li><a class="li" href="#">linkedin</a></li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script>
	feather.replace();
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip();
	})
</script>

</body>
</html>
